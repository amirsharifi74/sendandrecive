#include "clientclass.h"
#include <iostream>
clientClass::clientClass(QObject *parent) : QObject(parent)
{
    udpsocket = new QUdpSocket(this);
    udpsocket->bind(45454,QUdpSocket::ShareAddress);
    connect(udpsocket,SIGNAL(readyRead()),this,SLOT(recievedatagram()));
}

qint16 clientClass::getcppInt()
{
    return m_cppint;
    std::clog<<"cppint recived";
}

qint16 clientClass::getaverage()
{
    return m_average;
}

void clientClass::setCppInt(qint16 &a)
{
    m_cppint = a ;
    emit cppIntChanged();
}

void clientClass::setaverage(qint16 &b)
{
    m_cppint = b;
    emit averageChanged();
}

void clientClass::sendDatagram()
{
    mark.insert("mark",QJsonValue::fromVariant(m_cppint));
    QJsonDocument doc(mark);
    markbyte = doc.toBinaryData();
//    udpsocket->writeDatagram(markbyte,address,45454);
    udpsocket->writeDatagram(markbyte,QHostAddress::Broadcast,45454);
}

void clientClass::recievedatagram()
{
    while(udpsocket->hasPendingDatagrams()){
        datagram = udpsocket->receiveDatagram();
        avebyte.resize(int(udpsocket->pendingDatagramSize()));
        avebyte =datagram.data();
        doc=QJsonDocument::fromBinaryData(avebyte);
        aveobj = doc.object();
        avevalue = aveobj.value(QString("average"));
        m_average = avevalue.toInt();
        qDebug()<<"ave IS"<<m_average;
        setaverage(m_average);
    }
}

