#include "serverrqml.h"

serverrqml::serverrqml(QObject *parent) : QObject(parent)
{
    udpsocket = new QUdpSocket(this) ;
    udpsocket->bind(45454, QUdpSocket::ShareAddress);
//    udpsocket->bind(QHostAddress("192.168.10.10"),45454);
    connect(udpsocket,SIGNAL(readyRead()),this,SLOT(getdatagram()));
}

void serverrqml::getdatagram()
{

    while(udpsocket->hasPendingDatagrams()){
        datagram.resize(int (udpsocket->pendingDatagramSize()));
        QNetworkDatagram datagram = udpsocket->receiveDatagram();
        QByteArray a = datagram.data();
        doc = QJsonDocument::fromBinaryData(a);
        sett= doc.object();
        value= sett.value(QString("mark"));
        fromclientInt = value.toInt();
        sum+= fromclientInt;
        count++;
       averagenumm = serverrqml::average(sum,count);
       qDebug()<<"Datagram recived"<<averagenumm;
    }
}

int serverrqml::average(int num = 0,int howmany = 0)
{
    sum1 = num;
    count1 = howmany;
    ave = sum1 / count1 ;
    return ave;
}

void serverrqml::senddatagram()
{
    averageNum.insert("average",QJsonValue::fromVariant(averagenumm));
    QJsonDocument doc1(averageNum);
    qDebug()<<"Data send"<<doc1;
    datagram2 = doc1.toBinaryData();
    udpsocket->writeDatagram(datagram2,QHostAddress::Broadcast, 45454);
//    udpsocket->writeDatagram(datagram2,address,45454);
}

